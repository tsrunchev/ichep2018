% Please use the skeleton file you have received in the
% invitation-to-submit email, where your data are already
% filled in. Otherwise please make sure you insert your
% data according to the instructions in PoSauthmanual.pdf
\documentclass{PoS}
% \usepackage{subcaption}
\graphicspath{ {./Images/} }
\usepackage{wrapfig}
% \usepackage[final]{graphicx}
% \usepackage[backend=bibtex]{biblatex}

%     \DeclareBibliographyCategory{cited}
%     \AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}
    % \addbibresource{refs.bib}
%     \DeclareFieldFormat
%         [article,
%         inbook,
%         incollection,
%         inproceedings,
%         patent,
%         thesis, % also phdthesis
%         unpublished,
%         report, % also techreport
%         misc,
%         ]{title}{\href{\thefield{url}}{#1}}

\title{Emittance scans for CMS luminosity calibration}

\ShortTitle{CMS emittance scans}

\author{\speaker{Peter Tsrunchev}\thanks{Thanks to the members of the CMS Beam Radiation Instrumentation and Luminosity project and in particular Paul Lujan for presenting on the authors' behalf.}\, on behalf of the CMS collaboration\\
        CERN, Sofia University\\
        E-mail: \email{tsrunchev@outlook.com}}
\author{Olena Karacheban\\
       CERN\\
       E-mail: \email{olena.karacheban@cern.ch}}

\abstract{Emittance scans are short van der Meer type scans performed at the beginning and at the end of LHC fills.
The beams are scanned against each other in X and Y planes in 9 displacement steps and are used for LHC diagnostics and since 2017 for CMS luminosity calibration cross check.
An XY pair of scans takes less than 4 minutes elapsed time.
BRIL project provides to LHC three independent online luminosity measurement from PLT, BCM1F and HF.
The excellent performance of BRIL detectors, fast back-end electronics and CMS XDAQ based data processing and publication allow the use of emittance scans for linearity and stability studies of the luminometers.
Emittance scans became a powerful tool and dramatically improved understanding of luminosity measurement during the year.
Since each luminometer is independently calibrated in every scan the measurements are independent and ratios of luminometers can strictly be used as a final validation.
Two independent analyses of emittance scans are launched: offline python based framework and online XDAQ based application.
Results are published on the monitoring web-pages in real-time for the XDAQ based analysis and within typically 15 minutes for the python based framework, which has however the advantage of being rerunnable.
}

\FullConference{ICHEP 2018, International Conference on High Energy Physics\\
		4-11 July 2018\\
		Seoul, South Korea}

\begin{document}
\begin{figure}[]
    \vspace{-10pt}
    \centering
    \includegraphics[width=.32\textwidth]{sigvis(SBIL)_6362_bothscans.png}
    \includegraphics[width=.32\linewidth]{slope(Fill).png}
    \includegraphics[width=.32\textwidth]{stability.png}
    \caption{\textbf{Left:} PLT $\sigma_{vis}$ as a function of SBIL in Fill 6362, both scans. 
    \textbf{Center:} Per-scan slope of PLT $\sigma_{vis}(SBIL)$. 
    \textbf{Right:} Avg. PLT $\sigma_{vis}$ per fill for all data from 2017.}
\end{figure}
The CMS detector at the LHC has a number of systems to measure luminosity.
Every luminometer provides a counting rate $R$ proportional to the instantaneous luminosity $\mathcal{L}$ by a characteristic visible cross section $\sigma_{vis}=R/\mathcal{L}$.
It is calculated from beam parameters that are determined through a van der Meer (VdM) scan \cite{1} program conducted yearly as described in Ref. \cite{2}.

Since the start of 2017, at the beginning and end of each normal-conditions fill, a VdM-like scan, called an "emittance scan", consisting of 9 10-second steps is conducted.
Data from each bunch for each of the scans is fitted with a Single Gaussian shape, then $\sigma_{vis}$ and the Single Bunch Instantaneous Luminosity (SBIL) are calculated according to Ref. \cite{2}.

The per-bunch $\sigma_{vis}$ calculated from the scans in a single fill are studied as a function of SBIL.
Fig. 1, left shows such a dependency for leading and train bunches for Pixel Luminosity Telescope (PLT) detector, illustrated with data from fill 6362.
Slopes of the PLT nonlinearity per unit of SBIL obtained from linear fits are shown in the legend. 
A data driven simulation shows this slope is linearly dependent on the nonlinearity in the detector rates.
Emittance scans allow for a per-fill nonlinearity measurement based on these fits, as shown in Fig. 1, center.
Consistently higher nonlinearity is measured for leading bunches.
These slopes were studied for each luminometer and used as a nonlinearity correction to deliver final luminosity values. 

Under consistent conditions $\sigma_{vis}$ should be constant, so its fluctuations indicate changes in detector performance.
E.g. Fig. 1, right, shows $\sigma_{vis}$ for the whole year, and the highlighted areas correspond to periods where the PLT wasn't fully efficient.
Deficiencies were mitigated by raising the operational high voltage of the detector.
For fast feedback to LHC and CMS emittance scans are automatically analysed and results displayed on a web page as a quick reference check and as a long term stability monitor.

Emittance scans have proven to be a powerful tool for luminosity calibration and stability monitoring.
They were instrumental in decreasing the systematic uncertainty caused by detector signal nonlinearity in the final luminosity values for 2017.
They also continue to be utilised by CMS in 2018, and have also since been adopted by ATLAS.

\begin{thebibliography}{99}
  \bibitem{1}S. van der Meer,
  \emph{{Calibration of the effective beam height in the ISR}},
  CERN-ISR-PO-68-31,
  [{\tt https://cds.cern.ch/record/296752}]
\bibitem{2}CMS Collaboration,
\emph{{CMS luminosity measurement for the 2017 data-taking
period at $\sqrt{s} = 13~\mathrm{TeV}$}},
CMS-PAS-LUM-17-004
[{\tt https://cds.cern.ch/record/2621960}].

% ....

\end{thebibliography}
% \bibliographystyle{JHEP}
% \bibliography{refs}
\end{document}
